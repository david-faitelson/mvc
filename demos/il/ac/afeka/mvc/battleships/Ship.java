package il.ac.afeka.mvc.battleships;

import java.util.HashSet;
import java.util.Set;

public abstract class Ship {
	
	public abstract int size();
	
	public Ship() {
		
		parts = new HashSet<ShipPart>();
		
		for(int i = 0; i < size(); i++)
			parts.add(new ShipPart(this));
	}
	
	public boolean isSunk() {
		for(ShipPart p : parts) 
			if (p.isHit() == false)
				return false;
		
		return true;
	}
	
	public void placeOn(Grid grid, Location location, Direction direction) {
		if (direction == Direction.horizontal)
			placeHorizontallyOn(grid, location);
		else 
			placeVerticallyOn(grid, location);
	}
	
	public boolean canPlaceHorizontallyOn(Grid grid, Location location) {

		for(int i = -1 ; i < size() + 1; i++) {
			for (int j = -1; j <= 1; j++)
				if (grid.squareAt(location.add(i, j)) != Square.ocean)
					return false;
		}
		
		return true;
	}

	public boolean canPlaceVerticallyOn(Grid grid, Location location) {

		for(int i = -1 ; i < size() + 1; i++) {
			for (int j = -1; j <= 1; j++)
				if (grid.squareAt(location.add(j, i)) != Square.ocean)
					return false;
		}
		
		return true;
	}

	public void removeFrom(Grid grid) {
		for(ShipPart p : parts)
			p.removeFrom(grid);
	}
	
 	private void placeHorizontallyOn(Grid grid, Location location) {
 		int i = 0;
		for(ShipPart p : parts) {
			p.placeOn(grid, location.add(i,0));
			i++;
		} 
	}

	private void placeVerticallyOn(Grid grid, Location location) {
		int i = 0;
		for(ShipPart p : parts) {
			p.placeOn(grid, location.add(0,i));
			i++;
		}
	}

	private Set<ShipPart> parts;
}
