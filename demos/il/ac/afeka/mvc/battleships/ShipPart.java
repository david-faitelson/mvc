package il.ac.afeka.mvc.battleships;

public class ShipPart extends Square {

	public ShipPart(Ship ship) {
		this.ship = ship;
	}
	
	public void removeFrom(Grid grid) {
		grid.removePegFrom(location);
		location = null;
	}
	
	public void placeOn(Grid grid, Location l) {
		grid.place(this, l);
		location = l;
	}
	
	public boolean isHit() { return wasAttacked(); }
	
	public Shape shape() {
		if (ship.isSunk())
			return new SunkShape();
		else if (wasAttacked())
			return new HitShape();
		else
			return new ShipPartShape();
	}
	
	private Ship ship;
	private Location location;
}
