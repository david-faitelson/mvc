package il.ac.afeka.mvc.battleships;

import java.awt.Color;

import il.ac.afeka.geom.Point;
import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.graphics.Display;
import il.ac.afeka.input.Mouse;
import il.ac.afeka.mvc.View;

public class TrackingGridView extends View {
	
	public Location locationAtCursor() {
		
		Rectangle rect = displayTransformation().applyTo(getWindow());

		int nRows = model().numRows();
		int nCols = model().numCols();

		Double h = rect.getHeight() / nRows;
		Double w = rect.getWidth() / nCols;
		
		int row, col;

		Point p = Mouse.instance().cursorPoint().subtract(rect.getOrigin());
		
		row = (int)(p.getY() / h);
		col = (int)(p.getX() / w);
		
		return new Location(row, col);
	}
	
	public void displayView() {
		
		setBackgroundColor(Color.blue);
		
		displayGrid();
		displayPegs();
	}
	
	private void displayPegs() {

		int nRows = model().numRows();
		int nCols = model().numCols();
		
		for(int i = 0 ; i < nRows; i++)
		{
			for (int j = 0; j < nCols; j++)
			{
				drawSquareAt(model().squareAt(new Location(i,j)), i, j);
			}			
		}
	}
	
	private void drawSquareAt(Square square, int row, int col) {
		
		Rectangle rect = displayTransformation().applyTo(getWindow());

		int nRows = model().numRows();
		int nCols = model().numCols();

		Double h = row*rect.getHeight()/nRows;	
		Double w = col*rect.getWidth()/nCols;
		
		Point origin = rect.getOrigin().add(new Point(w, h));
		
		Rectangle s = new Rectangle(origin, origin.add(new Point(rect.getWidth()/nCols, rect.getHeight()/nRows)));
		
		s = s.insetBy(4.0);
		
		selectSquareShape(square).drawIn(s);
	}
	
	protected Shape selectSquareShape(Square aSquare) {
		return aSquare.shape();
	}

	private void displayGrid() {
		
		Rectangle rect = displayTransformation().applyTo(getWindow());

		int nRows = model().numRows();
		int nCols = model().numCols();
		
		for(int i = 0 ; i < nRows; i++)
		{
			Double h = i*rect.getHeight()/nRows;
			Point start = rect.getOrigin().add(new Point(0.0, h));
			Point end = rect.getOrigin().add(new Point(rect.getWidth(), h));
			Display.instance().drawLine(start, end, Color.black, 1);			
		}
		
		for (int i = 0; i < nCols; i++)
		{
			Double w = i*rect.getWidth()/nCols;
			Point start = rect.getOrigin().add(new Point(w, 0.0));
			Point end = rect.getOrigin().add(new Point(w, rect.getHeight()));
			Display.instance().drawLine(start, end, Color.black, 1);			
		}
		
	}

	private BattleshipsModel model() { return (BattleshipsModel)model; }
}
