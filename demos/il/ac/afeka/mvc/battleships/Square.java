package il.ac.afeka.mvc.battleships;

public abstract class Square {

	private boolean attacked = false;
	
	static public Square ocean = new Ocean();
	
	public void attack() { attacked = true; }
	
	public boolean wasAttacked() { return attacked; }
	
	public boolean isHit() { return false; }
		
	public Shape shape() { return new Shape(); }
	
}
