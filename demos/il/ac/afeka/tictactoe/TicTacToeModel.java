package il.ac.afeka.tictactoe;

import il.ac.afeka.mvc.Model;

public class TicTacToeModel extends Model {

	public static int EMPTY = 0;
	public static int X = 1;
	public static int O = 2;
	
	private int board[][];
	
	private int turn = X;
	
	public TicTacToeModel() {
		
		board = new int[3][3];
		
		clearBoard();
	}
	
	public void playAt(int row, int col) {
		
		atput(row, col, turn);
		
		if (turn == X) turn = O;
		else if (turn == O) turn = X;
		
	}
	
	public void clearBoard() {
		for(int i =0 ;i < 3; i++) {
			for (int j = 0; j < 3; j++)
				board[i][j] = EMPTY;
		}
	}
	
	public void atput(int row, int col, int piece) {
		if (board[row][col] == EMPTY) {
			board[row][col] = piece;
			notifyViews();
		}
	}

	public int pieceAt(int row, int col) {
		return board[row][col];
	}

}
