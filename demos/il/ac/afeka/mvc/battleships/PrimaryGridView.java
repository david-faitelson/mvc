package il.ac.afeka.mvc.battleships;

public class PrimaryGridView extends TrackingGridView {

	protected Shape selectSquareShape(Square aSquare) {
		if (aSquare.wasAttacked())
			return aSquare.shape();
		else
			return new Shape(); // does not draw anything
	}
}
