package il.ac.afeka.mvc.battleships;

public class Location {
	
	public Location(int x, int y) {
		col = x;
		row = y;
	}
	
	public Location add(int x, int y) {
		return new Location(x+col, y + row);
	}

	private int row, col;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Location [row=" + row + ", col=" + col + "]";
	}
}
