package il.ac.afeka.mvc.battleships;

import il.ac.afeka.input.Mouse;
import il.ac.afeka.mvc.Controller;

public class BattleshipsController extends Controller {

	public void controlActivity() {
		super.controlActivity();
				
		if (Mouse.instance().button1Pressed()) {
			
			Location loc = getView().locationAtCursor();
			
			getModel().shootAt(loc);
		}
		
	}
	
	public TrackingGridView getView() {
		return (TrackingGridView)super.getView();
	}

	public BattleshipsModel getModel() {
		return (BattleshipsModel)super.getModel();
	}
	
}
