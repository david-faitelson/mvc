package il.ac.afeka.mvc.battleships;

import java.awt.Color;

import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.graphics.Display;

public class HitShape extends Shape {

	@Override
	public void drawIn(Rectangle window) {
		Display.instance().fillRect(window, Color.orange);
		Display.instance().drawLine(window.getOrigin(), window.getCorner(), Color.black, 2);
		Display.instance().drawLine(window.getLowerLeft(), window.getUpperRight(), Color.black, 2);
	}

}
