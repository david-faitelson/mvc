package il.ac.afeka.tictactoe;

import java.awt.Color;

import il.ac.afeka.geom.Point;
import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.graphics.Display;
import il.ac.afeka.mvc.View;
import il.ac.afeka.mvc.views.StandardSystemView;

public class TicTacToeDemo {
	
	 public static void main(String[] args) {
		 
		 try {
			 View desktop = new View().windowViewport(new Rectangle(new Point (0.0,  0.0), new Point(1.0,1.0)), new Rectangle(new Point(100.0,100.0), new Point(400.0, 400.0)));
			 desktop.setBackgroundColor(Color.gray);
			 
			 StandardSystemView systemView = new StandardSystemView("Tic Tac Toe");
			 systemView.windowViewport(new Rectangle(new Point(0.0, 0.0), new Point(1.0, 1.0)), new Rectangle(new Point(0.25, 0.25), new Point(0.75,0.75)));
			 
			 TicTacToeModel model = new TicTacToeModel();
			 
			 TicTacToeBoardView buttonView = new TicTacToeBoardView(model);
			 
			 buttonView.windowViewport(new Rectangle(new Point(0.0, 0.0), new Point(1.0, 1.0)), new Rectangle(new Point(0.0, 0.0), new Point(1.0, 1.0)));
			 
			 systemView.addSubview(buttonView);			 
			 			 			 
			 desktop.addSubview(systemView);

			 desktop.display();

			 buttonView.setModel(model);

			 desktop.getController().startUp();
			 
			 try { Thread.sleep(1000); } catch(InterruptedException e) {} 

		} finally {
       	Display.instance().close();
       	System.exit(0);
       }
	 }
}

