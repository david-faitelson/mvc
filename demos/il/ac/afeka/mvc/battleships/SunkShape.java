package il.ac.afeka.mvc.battleships;

import java.awt.Color;

import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.graphics.Display;

public class SunkShape extends Shape {
	
	@Override
	public void drawIn(Rectangle window) {
		Display.instance().fillRect(window, Color.red);
		Display.instance().drawLine(window.getOrigin(), window.getCorner(), Color.black, 4);
		Display.instance().drawLine(window.getLowerLeft(), window.getUpperRight(), Color.black, 4);
	}
	
}
