package il.ac.afeka.mvc.battleships;

public class Miss extends Square {

	public Miss() {
		attack();
	}
	
	public Shape shape() {
		return new MissShape();
	}
}
