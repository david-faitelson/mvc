package il.ac.afeka.tictactoe;

import il.ac.afeka.input.Mouse;
import il.ac.afeka.mvc.Controller;

public class TicTacToeButtonController extends Controller {

	public void controlActivity() {
		super.controlActivity();
				
		if (Mouse.instance().button1Pressed()) {
			
			TicTacToeButtonView buttonView = (TicTacToeButtonView)getView();
			
			((TicTacToeModel)getModel()).playAt(buttonView.Row(), buttonView.Col());
		}
		
	}

}
