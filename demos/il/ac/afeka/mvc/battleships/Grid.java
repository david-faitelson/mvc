package il.ac.afeka.mvc.battleships;

import java.util.HashMap;
import java.util.Map;

public class Grid {
	
	public Grid() { squares = new HashMap<Location, Square>(); }
	
	public void place(Square square, Location location) {
		squares.put(location, square);
	}
	
	public void removePegFrom(Location location) {
		squares.remove(location);
	}
	
	public Square squareAt(Location location) {
	
		Square p = squares.get(location);
		if (p == null)
			return Square.ocean;
		return p;
	}
	
	public void hit(Location location) {
		Square p = squareAt(location);
		p.attack();
		if (!p.isHit()) {
			squares.put(location, new Miss());
		}
	}
	
	private Map<Location, Square> squares;
}
 