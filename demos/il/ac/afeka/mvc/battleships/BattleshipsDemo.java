package il.ac.afeka.mvc.battleships;

import java.awt.Color;

import il.ac.afeka.geom.Point;
import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.graphics.Display;
import il.ac.afeka.mvc.View;

public class BattleshipsDemo {

	public static void main(String[] args) {
		try {
			View view = new View().windowViewport(new Rectangle(new Point(0.0, 0.0), new Point(1.0, 1.0)),
					new Rectangle(new Point(100.0, 100.0), new Point(900.0, 700.0)));
			view.setBackgroundColor(Color.gray);

			BattleshipsModel aBattelshipsModel = new BattleshipsModel();
			
			PrimaryGridView aPrimaryGridView = new PrimaryGridView();

			aPrimaryGridView.windowViewport(new Rectangle(new Point(0.0, 0.0), new Point(1.0,1.0)), 
					new Rectangle(new Point(0.1,0.1), new Point(0.6,0.9)));
			
			BattleshipsController aBattelshipsController = new BattleshipsController();
			
			aPrimaryGridView.setModel(aBattelshipsModel);

			aPrimaryGridView.setController(aBattelshipsController);

			view.addSubview(aPrimaryGridView);

			TrackingGridView aPrimaryView = new TrackingGridView();

			aPrimaryView.windowViewport(new Rectangle(new Point(0.0, 0.0), new Point(1.0,1.0)), 
					new Rectangle(new Point(0.7,0.1), new Point(0.9,0.4)));
			
	
			aPrimaryView.setModel(aBattelshipsModel);

			view.addSubview(aPrimaryView);
			
			view.display();

			view.getController().startUp();

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}

		} catch(Exception err) {
			err.printStackTrace();
		}

		finally {
			Display.instance().close();
			System.exit(0);
		}
		
	}
}
