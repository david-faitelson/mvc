package il.ac.afeka.mvc.battleships;

import java.util.Vector;

import il.ac.afeka.mvc.Model;

public class BattleshipsModel extends Model {
	
	private Grid grid;
	private Vector<Ship> fleet;
	
	public BattleshipsModel() {
		buildFleet();
		placeShips();
	}
	
	private void buildFleet() {
		fleet = new Vector<Ship>();
		
		fleet.add(new Battleship());
		fleet.add(new Battleship());
		fleet.add(new Cruiser());
		fleet.add(new Cruiser());
		fleet.add(new Cruiser());
		fleet.add(new Submarine());
		fleet.add(new Submarine());
		fleet.add(new Submarine());
		fleet.add(new Destroyer());
		fleet.add(new Destroyer());
	}
	
	private void placeShips() {
		grid = new Grid();

		fleet.get(0).placeOn(grid, new Location(1,1), Direction.vertical);
		fleet.get(9).placeOn(grid, new Location(6,8), Direction.horizontal);
	}
	
	public int numRows() { return 10; }
	
	public int numCols() { return 10; }
	
	public Square squareAt(Location location) {
		return grid.squareAt(location);
	}
	
	public void shootAt(Location location) {
		grid.hit(location);
		notifyViews();
	}
}
