package il.ac.afeka.tictactoe;

import java.awt.Color;

import il.ac.afeka.geom.Point;
import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.graphics.Display;
import il.ac.afeka.mvc.View;

public class TicTacToeButtonView extends View {
	
	private int row, col;
	
	public TicTacToeButtonView(TicTacToeModel tttmodel, int row, int col) {
		this.row = row;
		this.col = col;
		setModel(tttmodel);
		setController(new TicTacToeButtonController());
	}

	public int Row() { return row; }
	public int Col() { return col; }
	
	private void displayX() {
		
		Rectangle rect = displayTransformation().applyTo(getWindow().insetBy(0.05));
		Display.instance().drawLine(rect.getOrigin(), rect.getCorner(), Color.black, 8);
		Display.instance().drawLine(rect.getLowerLeft(), rect.getUpperRight(), Color.black, 8);
	}
	
	private void displayO() {
		
		Rectangle rect = displayTransformation().applyTo(getWindow().insetBy(0.05));
		Display.instance().drawCircle(rect.getCenter(), Math.min(rect.getWidth()/2.0, rect.getHeight()/2), Color.black, 8);
	}
	
	public void displayView() {
		
		if (model != null) {
			int piece = ((TicTacToeModel)model).pieceAt(row, col);
			
			if (piece == TicTacToeModel.X)
				displayX();
			else if (piece == TicTacToeModel.O)
				displayO();
		}
	}

	public void update() {
		displayView();
	}
}
 