package il.ac.afeka.tictactoe;

import il.ac.afeka.geom.Point;
import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.mvc.View;

public class TicTacToeBoardView extends View {

	public TicTacToeBoardView(TicTacToeModel tttmodel) {
		
		for(int i =0; i < 3; i++)
			for(int j = 0; j < 3; j++) {
				TicTacToeButtonView buttonView = new TicTacToeButtonView(tttmodel, i,j);
				buttonView.windowViewport(new Rectangle(new Point(0.0,0.0), new Point(1.0, 1.0)), new Rectangle(new Point(i*0.3333, j*0.3333), new Point(i*0.3333+0.3333,j*0.3333+0.3333)));
				addSubview(buttonView);
			}
	}
}
