package il.ac.afeka.mvc.battleships;

import java.awt.Color;

import il.ac.afeka.geom.Rectangle;
import il.ac.afeka.graphics.Display;

public class ShipPartShape extends Shape {
	
	public void drawIn(Rectangle window) {
		Display.instance().fillRect(window, Color.gray);
	}

}
